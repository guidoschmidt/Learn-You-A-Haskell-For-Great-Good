--- Baby's first function, page 5

Simple function to double a number

> doubleMe x = x + x


Use `doubleMe`to double two numbers

> doubleUs x y = doubleMe x + doubleMe y


Use an if condition to double a number smaller than 100

> doubleSmallNumber x =
>   if x > 100
>     then x
>     else x * 2

> doubleSmallNumber' x = (if x > 100 then x else x * 2) + 1


Simple function that returns a string

> conanO'Brian = "It's me, Conan"

