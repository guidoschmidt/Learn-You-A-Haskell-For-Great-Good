--- Texas Ranges, page 13

Use ranges to create lists

> oneToTwenty = [1..20]
> alphabet = ['a'..'z']
> partOfABC = ['K'..'Q']


Specify a step to define the distance between the elemens

> oddOneToTwenty = [2,4..20]
> everyThird = [3,6..20]


Cycle a list

> iNeedACycledList = take 10 (cycle [1,2,3])
> giveMeLOLs x = take (x * 4) (cycle "LOL ")


Repeat an element as an infinite list

> allFives = take 10 (repeat 5)


You could also use replicate

> allSevens = replicate 10 7


Ranges mixed with floating point numbers can be evil!

> evilFloatingRange = [0.1,0.3..1]
