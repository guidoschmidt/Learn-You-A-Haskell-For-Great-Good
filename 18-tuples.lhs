--- Tuples, page 18

Tuples are heterogeneous, meaning their elements can contain different
kind of data

> tupl1 = (1, 3)
> tupl2 = (3, 'a', "Hello")
> tupl3 = (50, 50.5, "World", 'b')

Get the elements of a pair (2-pair-tuple)

> tupl1_frst = fst tupl1
> tupl1_scnd = snd tupl1


