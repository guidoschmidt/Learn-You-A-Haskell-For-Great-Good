--- List Comprehensions, page 15

They work like mathematical set definitions

> quadratices = [x * x | x <- [1..100]]

[ 1..100]   draw the values from the list from 1 to 100
x <-        x takes on the value of each element in that list (we bind each element in the list from 1 to 100 to x)
x * x       output of the list comprehension

> rest3 = [x | x <- [1..100], x `mod` 7 == 3]
> doubledBiggerOrEqual12 = [x * 2 | x <- [1..10], x * 2 >= 12]

x `mod` 7 == 3   is an additional condition, a predicate and is used to filter the list

> boomBanging xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]
> oddBoomBangers = boomBanging [7..13]

It is possible to include as many predicates as wanted

> not13or15or19 = [x | x <- [10..20], x /= 13, x /= 15, x /= 19]

Additionally, values can be drawn from more than one list

> xAndY = [x + y | x <- [1,2,3], y <- [10,100,1000]]

> nouns = ["hobo", "frog", "pope"]
> adjectives = ["lazy", "nice", "happy"]
> listOfThings = [adjective ++ " " ++ noun | adjective <- adjectives, noun <- nouns]


Use list comprehensions to implement the length function

> length' xs = sum [1 | _ <- xs]


Use list comprehensions to remove non-uppercase letters

> removeNonUppercaes st = [c | c <- st, c `elem` ['A'..'Z']]

