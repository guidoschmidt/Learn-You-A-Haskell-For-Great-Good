--- An Intro to Lists, page 7

Create a list

> lostNumbers = [4, 2, 1, 3, 12]


Concat two lists

> numbers = lostNumbers ++ [0, 7, 12]


Access list elements using !!

> firstElement = numbers !! 0
> firstLetter = "Guido" !! 0


Compare lists

> compareA = [3, 2, 1] > [2, 1, 0]
> compareB = [3, 2, 1] > [2, 10, 100]
> compareC = [3, 4, 2] < [3, 4, 3]
> compareD = [3, 4, 2] == [3, 4, 2]


The monster operations: head, tail, init, last

> monster = "Monster"
> headMonster = head monster
> tailMonster = tail monster
> initMonster = init monster
> lastMonster = last monster
> lengthMonster = length monster


null checks a list for emptyness

> checkIfEmpty = null []
> checkIfEmpty' = null "Notempty"


reverse a list

> reverseStuff = reverse "Guido"


take and drop element ranges from lists

> takeEm = take 3 [1, 2, 3, 4, 5]
> takeEm' = take 10 [1..]
> dropEm = drop 3 [1, 2, 3, 4, 5]
> dropEm' = drop 1 [1]


Sum all elements in a list

> calcSum = sum [1, 2, 3, 4, 5]
> calcProduct = product [1, 2, 3]


Check lists for containing elements

> hasIt3 l = 3 `elem` l
> hasIt7 l = 7 `elem` l
